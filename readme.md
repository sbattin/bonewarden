# Bone Warden (working title)

## Abstract
A platform game with simple qualities, with asepcts of Mario and Megaman.  The theme is to play as a skeleton who awakes in a pit littered with bones.  The bones can tumble and induce an avalanche effect.  The player throws, shoots, and magicks with bones.  Enemies involve skeletons and other, more potent undead.

## Important files:  
[Google doc with to-do lists:](https://docs.google.com/spreadsheet/ccc?key=0Ap7EFHYJjMaXdHVmWEZwa2FIdHVCTHJ4bmhBOE9CMFE)  
[Bitbucket git project](https://bitbucket.org/sbattin/bonewarden)  
Repo url: <https://bitbucket.org/sbattin/bonewarden>  

## Post-checkout instructions

There are some settings which are excluded from git because they reside in the IntelliJ project files.  Go through these steps to make the project functional after checkout.

1. import project from existing sources
2. in projectsettings->libraries->Mainlib->dependencies: select export
3. create desktop build configuration with android/assets/ as working directory

## High-level objects
Some objects which are public properties of the game class, serving as global exchanges.

### BoneInput (interface)
This is the set of functions that drives player interaction with the game.  It is instantiated differently for different platforms.  It also defines different control schemes, such as keyboard or controller.

### BoneWardFactory (interface)
Serves platform-appropriate configuration to a game object constructor.

### TextureManager
Loads texture atlas information and hosts Texture instances used by the Sprite class.

### LevelManager (Does too many things)
1. Loads all level data
2. keeps an instance of the current active level
3. Loads progress for a level and player save data
4. Draws a mesh for level ground
5. Retrieves floor elevation for player and other
6. (Planned) serves particle system for boneslides
7. Contains enemies
8. Contains weapon pool usages

### Other
* Settings manager
* Audio manager
* player-follow-camera
* G(lobal)Functions
* player
* sprite
* weapon
* screens

