package com.baction.bonewarden.main;

/**
 * Created by seth on 12/19/13.
 */
public interface BoneWardFactory {
    public BoneInput getInput();

    public LevelManager getLevelManager();

    public SettingsManager getSettingsManager();

    public TextureManager getTextureManager();

    public AudioManager getAudioManager();
}
