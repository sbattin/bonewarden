package com.baction.bonewarden.main;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by seth on 12/19/13.
 */
public class PlayScreen implements Screen {

    final BoneWarden game;
    PlayerFollowCamera camera;

    Player player;


    public PlayScreen(final BoneWarden bonewarden){
        game = bonewarden;

        camera = new PlayerFollowCamera();
        camera.setToOrtho(false, 800, 480);

        game.levelMan.load(bonewarden);


        player = new Player(bonewarden);
    }

    @Override
    public void show(){

    }
    @Override
    public void hide(){

    }

    @Override
    public void pause(){

    }
    @Override
    public void resume(){

    }

    @Override
    public void resize (int x, int y){

    }

    @Override
    public void render(float delta){

        // update
        camera.Follow(player.position, game.levelMan);
        camera.update();
        game.input.update(delta);
        player.update(delta, game.input, game.levelMan);
        game.levelMan.update(delta);

        // draw
        Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

        game.levelMan.draw(camera.combined);

        game.spriteBatch.setProjectionMatrix(camera.combined);
        game.spriteBatch.begin();
        game.levelMan.drawObjects(game.spriteBatch);
        player.draw(game.spriteBatch);
        game.spriteBatch.end();

    }

    @Override
    public void dispose(){

    }


}
