package com.baction.bonewarden.main;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by seth on 12/20/13.
 */
public class Weapon {

    public String name;
    public String spriteName;
    public float shotSpeed;
    public float shotDelay;
    public float gravitation;

    private Sprite sprite;
    public float collideHieght;

    public Weapon(String name, String spriteName, float shotSpeed, float shotDelay) {
        this.name = name;
        this.spriteName = spriteName;
        this.shotSpeed = shotSpeed;
        this.shotDelay = shotDelay;
        this.gravitation = 1;
        sprite = new Sprite(spriteName);

        collideHieght = sprite.size.y;
    }
    public void Load(TextureManager texMan){
        sprite.load(texMan);
    }
    public void Draw(SpriteBatch sb, float posX, float posY, float rot){
        sprite.draw(sb, posX, posY, rot);
    }



}
