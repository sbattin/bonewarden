package com.baction.bonewarden.main;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.ArrayMap;

import java.util.Set;

/**
 * Created by seth on 12/19/13.
 */
public class SettingsManager {

    String location;
    Skin skin;

    public float playerWalkSpeedMax;
    public float playerWalkAccel;
    public float playerRunSpeedMax;
    public float playerRunAccel;
    public float playerStopAccel;
    public float playerJumpSpeed;
    public float playerJumpSpeedRun;

    public Vector2 levelGravity;

    private ArrayMap<String, String> settings;

    public SettingsManager(){

        location = "settings.txt";

        playerWalkSpeedMax = 180;
        playerWalkAccel = 1800;
        playerRunSpeedMax = 240;
        playerRunAccel = 2000;
        playerStopAccel = 3600;
        playerJumpSpeed = 400;
        playerJumpSpeedRun = 480;

        levelGravity = new Vector2(0, -1000);

        settings = new ArrayMap<String, String>();
        settings.put("music", "1");
        settings.put("fun", "10");

    }

    public void save(){

        FileHandle fh = new FileHandle(location);


    }

    public void load(){

    }

    public Skin getSkin() {

        // A skin can be loaded via JSON or defined programmatically, either is fine. Using a skin is optional but strongly
        // recommended solely for the convenience of getting a texture, region, etc as a drawable, tinted drawable, etc.
        skin = new Skin();

        // Generate a 1x1 white texture and store it in the skin named "white".
        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        skin.add("white", new Texture(pixmap));

        // Store the default libgdx font under the name "default".
        skin.add("default", new BitmapFont());

        // Configure a TextButtonStyle and name it "default". Skin resources are stored by type, so this doesn't overwrite the font.

        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.up = skin.newDrawable("white", Color.DARK_GRAY);
        textButtonStyle.down = skin.newDrawable("white", Color.BLUE);
        textButtonStyle.checked = skin.newDrawable("white", Color.LIGHT_GRAY);
        textButtonStyle.over = skin.newDrawable("white", Color.LIGHT_GRAY);
        textButtonStyle.font = skin.getFont("default");
        skin.add("default", textButtonStyle);

        return skin;
    }
}
