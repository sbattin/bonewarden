package com.baction.bonewarden.main;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by seth on 12/27/13.
 */
public class PlayerFollowCamera extends OrthographicCamera {


    public void Follow(Vector2 position, LevelManager levelMan){
        if (this.position.x > (position.x + 150)){
            this.position.x = position.x + 150;
        }
        if (this.position.x < (position.x)){
            this.position.x = position.x;
        }
    }

}
