package com.baction.bonewarden.main;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by seth on 12/19/13.
 */
public interface BoneInput {

    public void update(float delta);

    public boolean newJump();
    public float move();
    public boolean run();
    public boolean shoot(Vector2 direction);
    public int changeWeapon();

    public boolean menuActiveSelect();
    public boolean menuMove(Vector2 direction);
    public boolean activate();


}
