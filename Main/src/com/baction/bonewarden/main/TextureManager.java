package com.baction.bonewarden.main;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

import java.util.ArrayList;


/**
 * Created by seth on 12/19/13.
 */
public class TextureManager implements Disposable{

    @Override
    public void dispose() {
        for (Texture t : tex){
            t.dispose();
        }
    }

    public static class TextureConfig implements Json.Serializable{
        public ArrayList<Sprite.SpriteConfig> spriteConfigs;
        public String[] textureNames;

        public TextureConfig(){
            spriteConfigs = new ArrayList<Sprite.SpriteConfig>();
        }

        public Sprite.SpriteConfig getSpriteConfig(String spriteName){
            for (Sprite.SpriteConfig sc : spriteConfigs){
                if (sc.name.equalsIgnoreCase(spriteName)){
                    return sc;
                }
            }
            throw new RuntimeException("Invalid sprite config name");
        }

        @Override
        public void write(Json json) {
            json.writeFields(this);
        }

        @Override
        public void read(Json json, JsonValue jsonData) {
            JsonValue sprites = jsonData.get("spriteConfigs");
            //for (JsonValue el = sprites.child(); el != null; el = el.next()){
            for (JsonValue el : sprites){
                Sprite.SpriteConfig sc = json.readValue(Sprite.SpriteConfig.class, el);
                this.spriteConfigs.add(sc);
            }
            textureNames = json.readValue("textureNames", String[].class, jsonData);

        }
    }


    /**
     * Load texture files by name.
     * load data associated with sprites by name
     * Generate
     */


    public Texture[] tex;
    private String[] textureNames;
    private TextureConfig config;

    public TextureManager(){}

    public void load(String configFile){


        FileHandle fh = Gdx.files.local(configFile);
        Json json = new Json();
        config = json.fromJson(TextureConfig.class, fh);



        textureNames = config.textureNames;
        tex = new Texture[textureNames.length];

        for (int i = 0; i < tex.length; i++){

            Texture t = new Texture(Gdx.files.internal(textureNames[i]));
            tex[i] = t;
        }

    }

    public ArrayMap<String,int[]> getAnimationIndexes(String spriteName) {
        ArrayMap<String, int[]> am = new ArrayMap<String, int[]>();

        int[] indicies = new int[2];
        indicies[0] = 0;
        indicies[1] = 1;
//        indicies[2] = 2;
//        indicies[3] = 3;
//        indicies[4] = 4;
        am.put(Sprite.ANIM_DEFAULT, new int[]{indicies[1]});
        am.put("jumping", new int[]{indicies[0]});
        am.put("walking", indicies);
        am.put("running", indicies);

        return am;
    }

    public ArrayMap<String,float[]> getAnimationTimes(String spriteName) {
        ArrayMap<String, float[]> am = new ArrayMap<String, float[]>();

        float[] times = new float[2];
        times[0] = 0.2f;
        times[1] = 0.2f;
//        times[2] = 0.2f;
//        times[3] = 0.2f;
//        times[4] = 0.2f;
        am.put(Sprite.ANIM_DEFAULT, new float[]{times[0]});
        am.put("jumping", new float[]{times[0]});
        am.put("walking", times);
        am.put("running", new float[]{times[0] / 2, times[1] / 2});

        return am;
    }

    public Rectangle[] getFrames(String spriteName) {
        Rectangle[] r = new Rectangle[5];
        r[0] = new Rectangle(0,0,28,56);
        r[1] = new Rectangle(34,0,28,56);
//        r[2] = new Rectangle(68,0,31,56);
//        r[3] = new Rectangle(106,0,34,56);
//        r[4] = new Rectangle(157,0,30,56);
        return r;
    }

    public int getTextureId(String spriteFileName) {
        for (int i = 0; i < tex.length; i++){
            if (textureNames[i].equalsIgnoreCase(spriteFileName)){
                return i;
            }
        }
        return -1;
    }

    public Sprite.SpriteConfig getSpriteConfig(String spriteName) {
        Sprite.SpriteConfig sc = config.getSpriteConfig(spriteName);
        sc.textureId = getTextureId(sc.textureFilename);
        return sc;
    }
}
