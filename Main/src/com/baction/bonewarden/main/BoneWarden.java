package com.baction.bonewarden.main;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

/**
 * Created by Seth on 12/18/13.
 */
public class BoneWarden extends Game {


    TextureManager texMan;
    AudioManager audioMan;
    LevelManager levelMan;
    SettingsManager settings;
    BoneInput input;

    SpriteBatch spriteBatch;

    BitmapFont font15;
    BitmapFont font22;

    public BoneWarden(BoneWardFactory factory){
        input = factory.getInput();
        levelMan = factory.getLevelManager();
        settings = factory.getSettingsManager();
        texMan = factory.getTextureManager();
        audioMan = factory.getAudioManager();
    }


    @Override
    public void create(){
        spriteBatch = new SpriteBatch();

        texMan.load("firstconfig.dat");

        levelMan.gravity = settings.levelGravity;

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("BONEAPA.TTF"));
        FreeTypeFontGenerator.FreeTypeBitmapFontData data;
        data = generator.generateData(15, FreeTypeFontGenerator.DEFAULT_CHARS, false);

        font15 = new BitmapFont(data, data.getTextureRegions(), false);
        data = generator.generateData(22, FreeTypeFontGenerator.DEFAULT_CHARS, false);
        font22 = new BitmapFont(data, data.getTextureRegions(), false);
        generator.dispose();

        //this.setScreen(new PlayScreen(this));
        this.setScreen(new MenuScreen(this));
    }

    @Override
    public void render() {
        super.render();
/*
        if(Gdx.input.isTouched()) {
            Vector3 touchPos = new Vector3();
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPos);
            skeleton.x = touchPos.x - 64 / 2;
        }

        if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) skeleton.x -= 200 * Gdx.graphics.getDeltaTime();
        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) skeleton.x += 200 * Gdx.graphics.getDeltaTime();


        spriteBatch.setProjectionMatrix(camera.combined);
        spriteBatch.begin();
        spriteBatch.draw(floorTex, floor.x, floor.y, floor.width, floor.height);
        spriteBatch.draw(skeletonTex, skeleton.x, skeleton.y, skeleton.width, skeleton.height);
        spriteBatch.end();
        */
    }

    @Override
    public void dispose() {

        spriteBatch.dispose();
        font15.dispose();
        font22.dispose();
        levelMan.dispose();
    }

}
