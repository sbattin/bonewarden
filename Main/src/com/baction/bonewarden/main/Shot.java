package com.baction.bonewarden.main;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;

/**
 * Created by seth on 12/28/13.
 */


public class Shot implements Pool.Poolable{
    public boolean active;
    public Weapon weapon;
    public Vector2 position;
    public Vector2 velocity;
    public float rotation;
    public float rotVelocity;

    public Shot() {
        active = false;
        weapon = null;
        position = Vector2.Zero;
        velocity = Vector2.Zero;
        rotation = 0;
        rotVelocity = 0;

    }

    public void init(Weapon weapon, Vector2 position, Vector2 velocity){
        this.active = true;
        this.weapon = weapon;
        this.position = position;
        this.velocity = velocity;
    }

    private Vector2 tempGravity;
    public void update(float delta, LevelManager levMan){

        if (!active || weapon == null){ return; }

        tempGravity = levMan.getGravity(position).cpy().scl(delta);
        tempGravity.scl(weapon.gravitation);
        velocity.add(tempGravity);

        position.x += velocity.x * delta;
        position.y += velocity.y * delta;
        rotation += rotVelocity * delta;

        if (position.y < -200){
            this.active = false;
        } else if (position.y < (levMan.getLocalElev(position) - weapon.collideHieght / 2)) {
            this.active = false;
        }
    }

    public void draw(SpriteBatch sb){
        if (weapon != null){
            weapon.Draw(sb, position.x, position.y, rotation);
        }
    }

    @Override
    public void reset() {
        active = false;
        weapon = null;
        position = Vector2.Zero;
        velocity = Vector2.Zero;
        rotation = 0;
        rotVelocity = 0;
    }
}