package com.baction.bonewarden.main;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by seth on 12/20/13.
 */
public class Player {

    public enum playerState {
        idle,
        twiddling,
        walking,
        running,
        jumping,
        cresting,
        falling
    }
    playerState state;

    Vector2 position;
    Vector2 velocity;
    float speed;

    float health;

    int actveWeapon;
    Weapon[] weapons;
    public Vector2 weaponDir;

    Sprite sprite;

    BoneWarden game;

    public Player(final BoneWarden game){
        this.game = game;
        state = playerState.idle;

        position = game.levelMan.getStartPosition();
        velocity = Vector2.Zero;
        health = 9001;
        speed = 100;

        actveWeapon = -1; //none?
        weapons = game.levelMan.getWeapons();
        actveWeapon = game.levelMan.getActiveWeapon();
        weaponDir = new Vector2();

        sprite = new Sprite("skeleton");
        sprite.position = position;
        sprite.size = new Vector2(50, 87);
        sprite.faceleft = true;
        sprite.load(game.texMan);
    }

    public void update(float delta, BoneInput input, LevelManager levelMan) {
        float move = input.move();
        boolean newJump = input.newJump();
        boolean isRunning = input.run();

        if (input.shoot(weaponDir)){
            if (!sprite.faceleft){
                weaponDir.scl(-1, 1);
            }
            levelMan.addShot(
                weapons[actveWeapon].name,
                position.cpy().add(-10, 0),
                weaponDir.cpy().add(velocity.cpy().nor().scl(0.3f))
            );
        }

        if (move > 0){
            sprite.faceleft = true;
        } else if (move < 0){
            sprite.faceleft = false;
        }
        switch (state){
            case idle:

                if (move != 0){
                    if (isRunning){
                        state = playerState.running;
                        sprite.setAnimation("running");
                    } else {
                        state = playerState.walking;
                        sprite.setAnimation("walking");
                    }
                } else if (newJump){
                    velocity.y = game.settings.playerJumpSpeed;
                    state = playerState.jumping;
                } else {
                    brake(delta);
                }

                break;
            case walking:
                if (newJump){
                    state = playerState.jumping;
                    sprite.setAnimation("jumping");
                    velocity.y = game.settings.playerJumpSpeed;
                }
                if (move != 0){
                    updateWalking(move, delta);
                } else {
                    brake(delta);
                }
                if (velocity.x == 0){
                    state = playerState.idle;
                    sprite.setAnimation(Sprite.ANIM_DEFAULT);
                }
                if (isRunning){
                    state = playerState.running;
                    sprite.setAnimation("running");
                }
                break;

            case running:
                if (newJump){
                    state = playerState.jumping;
                    sprite.setAnimation("jumping");
                    velocity.y = game.settings.playerJumpSpeedRun;
                }
                if (move != 0){
                    updateRunning(move, delta);
                } else {
                    brake(delta);
                }
                if (velocity.x == 0){
                    state = playerState.idle;
                    sprite.setAnimation(Sprite.ANIM_DEFAULT);
                }
                if (!isRunning){
                    state = playerState.walking;
                    sprite.setAnimation("walking");
                }
                break;
            case jumping:
                if (move != 0){
                    if (isRunning){
                        updateRunning(move, delta);
                    } else {
                        updateWalking(move / 2, delta);
                    }
                }
                if (velocity.y <= 0){
                    state = playerState.falling;
                }
                break;
            case falling:

                if (velocity.y == 0){
                    state = playerState.idle;
                }
                if (move != 0){
                    if (isRunning){
                        updateRunning(move, delta);
                    } else {
                        updateWalking(move / 2, delta);
                    }
                }
            default:
                break;

        }

        if (Math.abs(velocity.y ) > 1000){
            int i = 1;
        }

        updatePosition(delta);

        sprite.update(delta);


    }

    private void brake(float delta) {
        float slowdown = delta * game.settings.playerStopAccel *
            game.levelMan.getFriction (position, velocity);

        if (velocity.x > 0) {
            velocity.x = Math.max(0, velocity.x - slowdown);
        } else if (velocity.x < 0){
            velocity.x = Math.min(0, velocity.x + slowdown);
        }
    }

    private void updateWalking(float move, float delta) {
        updateLateral(move, delta, game.settings.playerWalkSpeedMax, game.settings.playerWalkAccel);
    }

    private void updateRunning(float move, float delta){
        updateLateral(move, delta, game.settings.playerRunSpeedMax, game.settings.playerRunAccel);

    }

    private void updateLateral(float move, float delta, float limit, float accel){

        velocity.x += (move * delta * accel);
        velocity.x = GFunctions.clamp(velocity.x, -1 * limit, limit);
    }


    private void updatePosition(float delta){

        position.x += velocity.x * delta;
        position.y += velocity.y * delta;

        float localEl = game.levelMan.getLocalElev(position);
        if (localEl > position.y){
            if ((localEl - position.y) < 15){
                position.y = localEl;
                velocity.y = 0;
                //state = playerState.idle;
                //sprite.setAnimation(Sprite.ANIM_DEFAULT);
            }
        } else if (position.y > localEl) {
            velocity.y += game.levelMan.getGravity(position).y * delta;
        }

    }

    public void draw(SpriteBatch sb){

        //sprite.position = position;
        sprite.draw(sb, position.x - 25, position.y, 0);
    }



}
