package com.baction.bonewarden.main;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;


/**
 * Created by seth on 12/20/13.
 */
public class MenuScreen implements Screen {

    OrthographicCamera camera;
    BoneWarden game;

    Stage menu;
    private boolean menuActive;

    Sprite test;

    public MenuScreen(final BoneWarden bonewarden){
        game = bonewarden;

        createMenu();


        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);

        test = new Sprite("skeleton");
        test.position = new Vector2(100,170);
        test.size = new Vector2(150, 260);
        test.faceleft = true;
        test.load(game.texMan);
        test.setAnimation("walking");
    }

    private void createMenu() {
        menu = new Stage();
        Gdx.input.setInputProcessor(menu);
        menuActive = false;

        Skin skin = game.settings.getSkin();

        Table table = new Table();
        menu.addActor(table);
        table.setPosition(100, 100);

        TextButton play = new TextButton("Play the game",skin);

        play.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.setScreen(new PlayScreen(game));
            }
        });

        TextButton quit = new TextButton("Quit to desktop",skin);
        quit.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.app.exit();
            }
        });

        ClickListener clickListener = new ClickListener(){
            public void enter(InputEvent event, int x, int y){
                System.out.println("mouse entered!");
            }
        };

        play.addListener(clickListener);
        quit.addListener(clickListener);

        table.add(play);
        table.add(quit);
    }


    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

        camera.update();
        test.update(delta);

        game.spriteBatch.setProjectionMatrix(camera.combined);

        game.spriteBatch.begin();
        game.font22.draw(game.spriteBatch, "Welcome to Bone Warden ?", 100, 150);
        test.draw(game.spriteBatch);
        if (!menuActive) {
            game.font15.draw(game.spriteBatch, "Press any key to begin", 100, 100);
        }

        game.spriteBatch.end();

        if (menuActive){

            menu.act();
            menu.draw();
        }


        if (!menuActive && game.input.activate()){
            menuActive = true;
            for (Actor a : menu.getActors()){
                a.setVisible(true);
            }
        }

    }

    @Override
    public void resize(int width, int height) {
        menu.setViewport(width, height, true);
    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        menu.dispose();
    }
}
