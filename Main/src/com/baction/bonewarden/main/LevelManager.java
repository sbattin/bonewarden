package com.baction.bonewarden.main;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Pool;

import java.util.ArrayList;

/**
 * Created by seth on 12/19/13.
 */
public class LevelManager implements Disposable{



    public class ProgressState{
        int levelOrdinal;
        int checkPoint;
        ArrayMap<Weapon, Boolean> weaponsAvail;
        int activeWeapon;
        int[] ammo;
        int lives;
        public ProgressState(){
            checkPoint = 0;
            weaponsAvail = new ArrayMap<Weapon, Boolean>();
            weaponsAvail.put(new Weapon("fists", null, 0, 0.5f), true);
            weaponsAvail.put(new Weapon("skull", "skull", 400, 0.75f), true);
            ammo = new int[] {-1, 50};
        }

        public Weapon[] getWeapons() {

            Weapon[] weapons = new Weapon[weaponsAvail.size];

            int current = 0;
            for (int i = 0; i < weapons.length; i++){
                if (weaponsAvail.getValueAt(i)){
                    weapons[i] = weaponsAvail.getKeyAt(i);
                    current++;
                }
            }
            return weapons;
        }

        public void Load(BoneWarden game) {
            for (Weapon w : getWeapons()){
                w.Load(game.texMan);
            }
        }
    }

    public class Level {
        public Vector2[] startPositions;
        public int ordinal;
        public float[] heights;
        public Level(){}

        public Level(Vector2[] starts, int ordinal){
            this.startPositions = starts;
            this.ordinal = ordinal;
            //heights = new int[17];
            heights = new float[] {
                100,
                80,
                90,
                95,
                110,
                105,
                105,
                115,
                120,
                100,
                90,
                85,
                80,
                75,
                100,
                120,
                138,
                153,
                165,
                163,
                160,
                150,
                135,
                115,
                110,
                105,
                102,
                100,
                    90,
                    85,
                    80,
                    75,
                    100,
                    120,
                    138,
                    153,
                    165,
                    163,
                    160,
                    150,
                    135,
                    115,
                    110,
                    105,
                    102,
                    100,
                    90,
                    85,
                    80,
                    75,
                    100,
                    120,
                    138,
                    153,
                    165,
                    163,
                    160,
                    150,
                    135,
                    115,
                    110,
                    105,
                    102,
                    100
            };
        }
    }

    public static final int SEGMENT_WIDTH = 32;

    public int currentLevel;
    private Level[] levels;
    public ProgressState progressState;
    public Vector2 gravity;

    public Mesh mesh;
    public ShaderProgram shader;
    public Texture floorTexture;

    private Pool<Shot> shotPool;
    private ArrayList<Shot> activeShots;

    public Enemy[] enemies;

    public Level getCurrentLevel() {
        return getLevel(currentLevel);
    }
    public Level getLevel(int index) {
        return levels[index];
    }

    public int getActiveWeapon() {
        return progressState.activeWeapon;
    }

    public LevelManager(){

        shotPool = new Pool<Shot>() {
            @Override
            protected Shot newObject() {
                return new Shot();
            }
        };
        activeShots = new ArrayList<Shot>();

        enemies = new Enemy[2];

    }

    public void load(BoneWarden game){

        LoadLevels();
        LoadProgress(game);

        LoadEnemies(game);

        MakeMesh();
        MakeShader();
    }

    private void LoadEnemies(BoneWarden game) {

        enemies[0] = new Enemy(new Vector2(200, 200), new Sprite("skeleton"));
        enemies[0].sprite.load(game.texMan);
        enemies[1] = new Enemy(new Vector2(400, 150), new Sprite("skeleton"));
        enemies[1].sprite.load(game.texMan);
    }

    private void LoadLevels() {

        levels = new Level[1];
        Level l = new Level( new Vector2[] {new Vector2(100,100)}, 1);
        levels[0] = l;


    }

    private void MakeMesh() {


        float[] verts = new float[10 * levels[0].heights.length];

        for (int i = 1; i <= (levels[0].heights.length); i++){

            verts[10 * i - 10] = (i - 1) * SEGMENT_WIDTH;
            verts[10 * i - 9] = levels[0].heights[i - 1];
            verts[10 * i - 8] = 0;
            verts[10 * i - 7] = (i - 1);
            verts[10 * i - 6] = 0;

            verts[10 * i - 5] = (i - 1) * SEGMENT_WIDTH;
            verts[10 * i - 4] = 0;
            verts[10 * i - 3] = 0;
            verts[10 * i - 2] = (i - 1);
            verts[10 * i - 1] = levels[0].heights[i - 1] / SEGMENT_WIDTH;

        }

        short[] indices = new short[6 * levels[0].heights.length];

        for (short i = 1; i < levels[0].heights.length; i++) {
            indices[6 * i - 6] = (short) (i * 2 - 2);
            indices[6 * i - 5] = (short) (i * 2 - 1);
            indices[6 * i - 4] = (short) (i * 2 - 0);
            indices[6 * i - 3] = (short) (i * 2 - 1);
            indices[6 * i - 2] = (short) (i * 2 - 0);
            indices[6 * i - 1] = (short) (i * 2 + 1);
        }


        mesh = new Mesh(Mesh.VertexDataType.VertexArray, true, verts.length / 5, indices.length,
                VertexAttribute.Position(),
            //new VertexAttribute(VertexAttributes.Usage.Position, 3, "a_position"),
              VertexAttribute.TexCoords(0));
        mesh.setVertices(verts);
        mesh.setIndices(indices);

    }

    private void MakeShader() {
        String vertexShader =
            "attribute vec3 a_position;    \n" +
            "attribute vec2 a_texCoord0;\n" +
            "uniform mat4 u_worldView;\n" +
            "varying vec2 v_texCoords;" +
            "void main()                  \n" +
            "{                            \n" +
            "   v_texCoords = a_texCoord0; \n" +
            "   gl_Position =  u_worldView * vec4(a_position, 1);  \n"      +
            "}                            \n" ;
        String fragmentShader = "#ifdef GL_ES\n" +
            "precision mediump float;\n" +
            "#endif\n" +
            "varying vec2 v_texCoords;\n" +
            "uniform sampler2D u_texture;\n" +
            "void main()                        \n" +
            "{                                  \n" +
            "  gl_FragColor = texture2D(u_texture, v_texCoords);\n" +
        "}";

        shader = new ShaderProgram(vertexShader, fragmentShader);

        if (!shader.isCompiled()){
            System.out.println(shader.getLog());
        }

        floorTexture = new Texture(Gdx.files.internal("bison2.png"));
    }
    private void LoadProgress(BoneWarden game) {
        //TODO: load from disk, or something like that.
        progressState = new ProgressState();
        progressState.activeWeapon = 1;
        progressState.Load(game);
        this.currentLevel = 0;
    }


    public Vector2 getStartPosition() {
        return getCurrentLevel().startPositions[progressState.checkPoint];
    }

    public Weapon[] getWeapons() {
        return progressState.getWeapons();
    }

    public float getFriction(Vector2 position, Vector2 velocity){
        return 1;
    }
    //TODO: new gravity setup...
    public Vector2 getGravity(Vector2 position){
        return gravity;
    }

    public float getLocalElev(Vector2 position){

        if ( (position.x < 0) || (position.x > SEGMENT_WIDTH * (levels[currentLevel].heights.length - 1))){
            return 100;
        }

        float first = levels[currentLevel].heights[(int)Math.floor(position.x / SEGMENT_WIDTH)];
        float second = levels[currentLevel].heights[(int)Math.ceil(position.x / SEGMENT_WIDTH)];

        return first + (second - first) * (position.x % SEGMENT_WIDTH) / SEGMENT_WIDTH;

    }

    public void addShot(String weapName, Vector2 position, Vector2 weaponDir) {
        for (Weapon w : getWeapons()){
            if (w.name.equalsIgnoreCase(weapName)){
                addShot(w, position, weaponDir);
                return;
            }
        }
        throw new RuntimeException("Invalid weapon name.");
    }
    public void addShot(Weapon weapon, Vector2 position, Vector2 weaponDir){
        Shot shot = shotPool.obtain();
        shot.init(weapon, position, weaponDir.scl(weapon.shotSpeed));
        activeShots.add(shot);
    }

    public void update(float delta){
        Shot shot;
        for (int i = activeShots.size() - 1; i >= 0; i--) {
            shot = activeShots.get(i);
            if (shot.active){
                shot.update(delta, this);
            } else {
                activeShots.remove(i);
                shotPool.free(shot);
            }
        }
        for (Enemy e : enemies){
            e.update(delta);
        }
    }

    public void draw(Matrix4 worldView){

        floorTexture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        floorTexture.bind(0);

        shader.begin();
        shader.setUniformMatrix("u_worldView", worldView);
        shader.setUniformi("u_texture", 0);
        mesh.render(shader, GL20.GL_TRIANGLES);
        shader.end();
    }

    public void drawObjects(SpriteBatch sb){
        for (Shot s : activeShots){
            if (s.active){
                s.draw(sb);
            } // else {to be removed}
        }
        for (Enemy e : enemies){
            e.draw(sb);
        }
    }


    @Override
    public void dispose() {
        mesh.dispose();
        shader.dispose();
        floorTexture.dispose();
    }
}
