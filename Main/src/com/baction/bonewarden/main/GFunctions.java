package com.baction.bonewarden.main;

/**
 * Created by seth on 12/22/13.
 */
public class GFunctions {
    public static float clamp(float val, float min, float max){
        return Math.min(max,Math.max(val, min));
    }
}
