package com.baction.bonewarden.main;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by seth on 1/4/14.
 */
public class Enemy {
    public Vector2 position;
    public Sprite sprite;

    public Enemy(Vector2 position, Sprite sprite){
        this.position = position;
        this.sprite = sprite;
    }

    public void update(float delta){
        sprite.update(delta);
    }

    public void draw(SpriteBatch sb){
        sprite.draw(sb, position.x, position.y, 0);
    }
}
