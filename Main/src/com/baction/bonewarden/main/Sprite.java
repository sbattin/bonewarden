package com.baction.bonewarden.main;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import org.w3c.dom.css.Rect;

import java.util.ArrayList;

/**
 * Created by seth on 12/20/13.
 */
public class Sprite {

    public static class SpriteConfig implements Json.Serializable {
        public String name;
        public int textureId;
        public String textureFilename;
        public ArrayList<Rectangle> frames;
        public ArrayMap<String, int[]> animationIndices;
        public ArrayMap<String, float[]> animationTimes;

        public int getTextureId(){
            return textureId;
        }
        public ArrayMap<String, int[]> getAnimationIndices() {return animationIndices; }
        public ArrayMap<String, float[]> getAnimationTimes() { return animationTimes; }
        public Rectangle[] getFrames(){
            Rectangle[] result = new Rectangle[frames.size()];
            for (int i = 0; i < frames.size(); i++){
                result[i] = frames.get(i);
            }
            return result;
        }

        public SpriteConfig(){
            this.frames = new ArrayList<Rectangle>();
            this.animationIndices = new ArrayMap<String, int[]>();
            this.animationTimes = new ArrayMap<String, float[]>();
        }


        @Override
        public void write(Json json) {
            //json.setElementType(SpriteConfig.class, "rectangle", Rectangle.class);
            json.writeValue("name", name);
            json.writeValue("frames", frames, frames.getClass(), Rectangle.class);
            json.writeValue("textureFilename", textureFilename);
            json.writeValue("animationIndices", animationIndices);
            json.writeValue("animationTimes", animationTimes);
        }

        @Override
        public void read(Json json, JsonValue jsonData) {
            this.name = jsonData.getString("name");
            this.textureFilename = jsonData.getString("textureFilename");
            for (JsonValue f : jsonData.get("frames")){
                frames.add(json.readValue(Rectangle.class, f));
            }
            for (JsonValue i : jsonData.get("animationIndices")){
                animationIndices.put(i.name, json.readValue(int[].class, i));
            }
            for (JsonValue t : jsonData.get("animationTimes")){
                animationTimes.put(t.name, json.readValue(float[].class, t));
            }
        }
    }

    public enum AnimationTypes{
        STILL,
        LOOP,
        LOOPBACK,
        FORWARD,
        BACKWARD,
        FORWARDBACKWARD
    };

    public static final String ANIM_DEFAULT = "default";
    private TextureManager tMan;

    // settables
    private String animation; // public void setAnimation(String name)
    public AnimationTypes animationType;
    public int animationDirection = 1;
    public Vector2 position;
    public Vector2 origin;
    public Vector2 size;
    public Vector2 scale;
    public float rotation;
    public boolean faceleft;
    public boolean upsidedown;

    // animation status
    private int frameIndex;
    private float frameTime;
    private int animationMapIndex;

    // loaded data
    private String spriteName;
    private int textureID;
    private int spriteID;
    private String[] animations;
    private ArrayMap<String, int[]> animationIndexes;
    private ArrayMap<String, float[]> animationTimes;
    private Rectangle[] frames;
    private boolean loaded;

    // cached data
    private Rectangle currentFrame;
    private int currentIndex;
    private float timeCheck;
    private int frameCount;


    public Sprite(String spriteName){

        this.spriteName = spriteName;
        animationType = AnimationTypes.LOOP;
        animation = ANIM_DEFAULT;
        position = Vector2.Zero;
        origin = Vector2.Zero;
        size = new Vector2(50, 50); //had to be something
        scale = new Vector2(1, 1);
        rotation = 0;
        faceleft = false;
        upsidedown = false;
    }

    public boolean isLoaded(){
        return loaded;
    }

    public void load(final TextureManager tMan){

        this.tMan = tMan;

        if (spriteName == null){
            System.out.println("Sprite load: aborted due to empty filename.");
            return;
        }

        SpriteConfig config = tMan.getSpriteConfig(spriteName);

        textureID = config.getTextureId();
        animationIndexes = config.getAnimationIndices();
        animationTimes = config.getAnimationTimes();
        frames = config.getFrames();


        setAnimation(animation);

        loaded = true;

    }

    public void setAnimation(String name){
        if (animationIndexes.containsKey(name)){
            animation = name;
            animationMapIndex = animationIndexes.indexOfKey(name);
            frameCount = animationIndexes.getValueAt(animationMapIndex).length;
            frameIndex = -1;
            nextFrame();
        }
    }

    public void update(float delta)
    {
        if (!loaded){ return; }
        frameTime += delta;

        while (frameTime > timeCheck){
            frameTime -= timeCheck;
            if (frameTime < timeCheck){
                nextFrame();
            }
        }

    }

    private void nextFrame(){
        if (frameIndex == -1){
            frameIndex = 0;
        } else {
            switch (animationType){
                case LOOP:
                    frameIndex++;
                    if (frameIndex >= frameCount){ frameIndex = 0;}
                    break;
                case LOOPBACK:
                    frameIndex--;
                    if (frameIndex < 0){ frameIndex = frameCount - 1;}
                    break;
                case FORWARD:
                    frameIndex++;
                    if (frameIndex >= frameCount){ frameIndex = frameCount - 1;}
                    break;
                case BACKWARD:
                    if (frameIndex > 0){ frameIndex--;}
                    break;
                case FORWARDBACKWARD:
                    frameIndex += animationDirection;
                    if (frameIndex < 0 || frameIndex == frameCount){
                        animationDirection *= -1;
                        frameIndex += animationDirection;
                    }
                    break;
                case STILL:
                default:
                    frameIndex = 0;
                    break;
            }
        }
        timeCheck = animationTimes.getValueAt(animationMapIndex)[frameIndex];
        currentIndex = animationIndexes.getValueAt(animationMapIndex)[frameIndex];
        currentFrame = frames[currentIndex];
    }

    public void draw(SpriteBatch sb, float posX, float posY, float rot){

        if (!loaded){ return;}  // necessary?  Who knows!!?!?

        sb.draw(tMan.tex[textureID],
                posX, posY,
                origin.x, origin.y,
                size.x, size.y,
                scale.x, scale.y,
                rot,
                (int)currentFrame.x,
                (int)currentFrame.y,
                (int)currentFrame.width,
                (int)currentFrame.height,
                faceleft,upsidedown
        );
    }

    public void draw(SpriteBatch sb){
        draw(sb, position.x, position.y, rotation);
    }



}
