package com.baction.bonewarden.Android;

import android.os.Bundle;
import com.baction.bonewarden.main.BoneWarden;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class BoneWardenAndroidStarter extends AndroidApplication {
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
        cfg.useAccelerometer = false;
        cfg.useCompass = false;
        cfg.useWakelock = true;
        cfg.useGL20 = true;
        initialize(new BoneWarden(new AndroidFactory()), cfg);
    }
}