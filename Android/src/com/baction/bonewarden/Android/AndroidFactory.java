package com.baction.bonewarden.Android;

import com.baction.bonewarden.main.*;

/**
 * Created by seth on 12/21/13.
 */
public class AndroidFactory implements BoneWardFactory {
    @Override
    public BoneInput getInput() {
        return null;
    }

    @Override
    public LevelManager getLevelManager() {
        return null;
    }

    @Override
    public SettingsManager getSettingsManager() {
        return null;
    }

    @Override
    public TextureManager getTextureManager() {
        return null;
    }

    @Override
    public AudioManager getAudioManager() {
        return null;
    }
}
