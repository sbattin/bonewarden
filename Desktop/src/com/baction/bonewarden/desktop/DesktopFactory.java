package com.baction.bonewarden.desktop;

import com.baction.bonewarden.main.*;

/**
 * Created by seth on 12/19/13.
 */
public class DesktopFactory implements BoneWardFactory {

    private DesktopBoneInput input;
    private LevelManager level;
    private SettingsManager settings;
    private TextureManager textures;
    private AudioManager audio;

    public DesktopFactory(){
        input = new DesktopBoneInput();
        level = new LevelManager();
        settings = new SettingsManager();
        textures = new TextureManager();
        audio = new AudioManager();
    }

    @Override
    public BoneInput getInput() {
        return input;
    }

    @Override
    public LevelManager getLevelManager() {
        return level;
    }

    @Override
    public SettingsManager getSettingsManager() {
        return settings;
    }

    @Override
    public TextureManager getTextureManager() {
        return textures;
    }

    @Override
    public AudioManager getAudioManager() {
        return audio;
    }
}
