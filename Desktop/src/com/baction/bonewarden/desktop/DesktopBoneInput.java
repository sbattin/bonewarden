package com.baction.bonewarden.desktop;
import com.baction.bonewarden.main.BoneInput;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by seth on 12/19/13.
 */
public class DesktopBoneInput implements BoneInput{

    public class InputState{
        boolean lastJumpKey;
        boolean lastShootKey;

        public void copyTo(InputState other){
            other.lastJumpKey = this.lastJumpKey;
            other.lastShootKey = this.lastShootKey;
        }
    }
    public InputState inputState;
    public InputState inputStatePrev;

    public DesktopBoneInput(){
        inputState = new InputState();
        inputStatePrev = new InputState();
    }


    @Override
    public void update(float delta) {
        inputState.copyTo(inputStatePrev);
        if (Gdx.input.isKeyPressed(Input.Keys.SPACE)){
            inputState.lastJumpKey = true;
        } else {
            inputState.lastJumpKey = false;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT)){
            inputState.lastShootKey = true;
        } else {
            inputState.lastShootKey = false;
        }

    }

    @Override
    public boolean newJump(){
        if (inputState.lastJumpKey && !inputStatePrev.lastJumpKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public float move() {

//        float val = 0;
//        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
//            val += 1;
//        }
//        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)){
//            val -= 1;
//        }
//        return val;

        return
            (Gdx.input.isKeyPressed(Input.Keys.RIGHT) ? 1 : 0) +
            (Gdx.input.isKeyPressed(Input.Keys.LEFT) ? -1 : 0);
    }

    @Override
    public boolean run() {
        return (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT));
    }

    @Override
    public boolean shoot(Vector2 direction) {

        if (inputState.lastShootKey && !inputStatePrev.lastShootKey) {
            direction.set(0.5f, 0.866f);
            return true;
        } else {
            direction.set(0,0);
            return false;
        }
    }

    @Override
    public int changeWeapon() {
        return 0;
    }

    @Override
    public boolean menuActiveSelect() {
        return Gdx.input.isKeyPressed(Input.Keys.SPACE);
    }

    @Override
    public boolean menuMove(Vector2 direction) {
        return false;
    }

    @Override
    public boolean activate() {
        return Gdx.input.isKeyPressed(Input.Keys.ANY_KEY);
    }


}
