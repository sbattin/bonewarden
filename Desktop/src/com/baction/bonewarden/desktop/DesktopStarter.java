package com.baction.bonewarden.desktop;
import com.baction.bonewarden.main.BoneWarden;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

/**
 * Created by Seth on 12/18/13.
 */
public class DesktopStarter {
    public static void main(String[] args) {
        LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
        cfg.title = "Bone Warden";
        cfg.useGL20 = true;
        cfg.width = 800;
        cfg.height = 480;

        new LwjglApplication(new BoneWarden(new DesktopFactory()), cfg);
    }
}
